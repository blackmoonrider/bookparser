﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookParserDb
{
    public class Log
    {
        public int Id { get; private set; }

        public DateTime Date { get; set; }

        [Required]
        public string Message { get; set; }
    }
}
