﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BookParserDb
{
    public class BookParserContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Log> Logs { get; set; }
        public BookParserContext() : base("BookAppDbConnection") { }
    }
}
