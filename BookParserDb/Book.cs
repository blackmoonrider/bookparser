﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookParserDb
{
    public class Book
    {
        [Key, Column(Order = 0)]
        public string WriterFirstName { get; set; }

        [Key, Column(Order = 1)]
        public string WriterLastName { get; set; }

        [Key, Column(Order = 2)]
        public string Name { get; set; }

        public DateTime PublicationDate { get; set; }

        public int Pages { get; set; }

        public int Raiting { get; set; }

        public decimal Price { get; set; }
    }
}
