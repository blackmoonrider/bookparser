using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace BookParser
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigInput configIO = (ConfigInput)ConfigurationManager.GetSection("ConfigInput");
            ConfigOutput configApp = (ConfigOutput)ConfigurationManager.GetSection("ConfigOutput");

            Application application = new Application(configIO, configApp);

            try
            {
                application.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error has occured: \r\n{ex.Message}");
                Console.WriteLine($"Press any key to exit the application.");
                Console.ReadKey();
            }
        }
    }
}
