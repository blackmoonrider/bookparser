﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace BookParser
{
    class ConfigOutput : ConfigurationSection
    {
        [ConfigurationProperty("createFilePath")]
        [StringValidator(InvalidCharacters = "|<>*\"?:")]
        public string CreateFilePath
        {
            get { return (string)this["createFilePath"]; }
            set { this["createFilePath"] = value; }
        }

        [ConfigurationProperty("targetPopularityRating")]
        [IntegerValidator(MinValue = 0, MaxValue = 9)]
        public int TargetPopularityRating
        {
            get { return (int)this["targetPopularityRating"]; }
            set { this["targetPopularityRating"] = value; }
        }
    }
}
