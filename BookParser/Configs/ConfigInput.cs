﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace BookParser
{
    class ConfigInput : ConfigurationSection
    {
        [ConfigurationProperty("loadFilePath")]
        [StringValidator(InvalidCharacters = "|<>*\"?:")]
        public string LoadFilePath
        {
            get { return (string)this["loadFilePath"]; }
            set { this["loadFilePath"] = value; }
        }
    }
}
