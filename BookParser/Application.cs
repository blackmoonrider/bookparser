﻿using BookParserDb;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BookParser
{
    class Application
    {
        private readonly ConfigInput configInput;
        private readonly ConfigOutput configOutput;

        public Application(ConfigInput configInput, ConfigOutput configOutput)
        {
            this.configInput = configInput;
            this.configOutput = configOutput;
        }

        public void Run()
        {
            RenderMenu(message: String.Empty);

            while (true)
            {
                ManageUserInput();
            }
        }

        private void RenderMenu(string message)
        {
            Console.Clear();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("[1] Load file");
            stringBuilder.AppendLine("[2] Create order");
            stringBuilder.AppendLine("[Q] Quit application");
            if (!String.IsNullOrWhiteSpace(message))
            {
                stringBuilder.AppendLine();
                stringBuilder.AppendLine(message);
            }
            Console.WriteLine(stringBuilder.ToString());
        }

        private void ManageUserInput()
        {
            ConsoleKeyInfo userKeyPress = Console.ReadKey();

            switch (userKeyPress.Key)
            {
                case ConsoleKey.D1:
                    try
                    {
                        string path = Path.GetFullPath($"{configInput.LoadFilePath}\\booklist_{DateTime.Now.ToString("yyyyMMdd")}.csv");

                        if (!File.Exists(path))
                        {
                            RenderMenu($"The {path} file does not exist. Press any key to exit.");
                            Console.ReadKey();
                            Environment.Exit(0);
                        }

                        RenderMenu($"Reading the {path} file...");
                        IEnumerable<string> textLines = LoadFile(path).Skip(1);

                        RenderMenu($"Populating the database...");
                        PopulateDb(textLines);

                        RenderMenu("Done! The database has been successfully populated.");
                    }
                    catch
                    {
                        throw;
                    }
                    break;
                case ConsoleKey.D2:
                    try
                    {
                        RenderMenu($"Saving the orders...");
                        SaveOrders();

                        RenderMenu("Done! The orders file has been successfully created.");
                    }
                    catch
                    {
                        throw;
                    }
                    break;
                case ConsoleKey.Q:
                    RenderMenu($"Bye!..");
                    Environment.Exit(0);
                    break;
                default:
                    break;
            }
        }

        private IEnumerable<string> LoadFile(string path)
        {
            using (StreamReader streamReader = new StreamReader(path))
            {
                while (!streamReader.EndOfStream)
                {
                    yield return streamReader.ReadLine();
                }
            }
        }

        private List<string> ParseSCSV(string inputString)
        {
            return inputString.Split(new char[] { ';' }).ToList();
        }

        private void PopulateDb(IEnumerable<string> collection)
        {
            using (BookParserContext context = new BookParserContext())
            {
                foreach (string item in collection)
                {
                    List<string> itemParts = ParseSCSV(item);

                    Book book = new Book
                    {
                        Name = itemParts[0],
                        WriterFirstName = itemParts[1],
                        WriterLastName = itemParts[2],
                        PublicationDate = DateTime.Parse(itemParts[3]),
                        Pages = int.Parse(itemParts[4]),
                        Raiting = int.Parse(itemParts[5]),
                        Price = decimal.Parse(itemParts[6])
                    };

                    context.Books.AddOrUpdate(book);
                }

                context.Logs.Add(
                    new Log()
                    {
                        Date = DateTime.Now,
                        Message = "Start populating the database"
                    }
                );

                context.SaveChanges();

                context.SaveChanges();

                context.Logs.Add(
                    new Log()
                    {
                        Date = DateTime.Now,
                        Message = "Populating successfully finished"
                    }
                );

                context.SaveChanges();
            }
        }

        private void SaveOrders()
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Orders));
            
            using (BookParserContext context = new BookParserContext())
            {
                Orders orders = CreateOrders(FilterBooks(context.Books, configOutput.TargetPopularityRating));

                string path = configOutput.CreateFilePath;

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                using (var stream = File.OpenWrite($"{path}\\{orders.OrderNumber}.xml"))
                {
                    xmlSerializer.Serialize(stream, orders);
                }
            }
        }

        private List<Book> FilterBooks(DbSet<Book> books, int targetPopularityRating)
        {
            return books
                .Where(b => b.Raiting > targetPopularityRating)
                .OrderBy(b => b.Price / b.Pages)
                .Take(100)
                .ToList();
        }

        private Orders CreateOrders(List<Book> books)
        {
            List<OrderElement> orderElements = new List<OrderElement>();

            foreach (Book book in books)
            {
                BookDetails bookDetails = new BookDetails()
                {
                    Publishdate = book.PublicationDate.ToString("yyyy-MM-dd"),
                    PagePrice = (decimal)book.Price / book.Pages
                };

                OrderElement orderElement = new OrderElement()
                {
                    Book_Name = book.Name,
                    Writer_FisrtName = book.WriterFirstName,
                    Writer_LastName = book.WriterLastName,
                    Book_details = bookDetails
                };

                orderElements.Add(orderElement);
            }

            Orders orders = new Orders()
            {
                OrderNumber = $"Order_{DateTime.Now.ToString("yyyyMMdd")}",
                DocDate = DateTime.Now.ToString("yyyyMMdd"),
                OrderElements = orderElements
            };

            return orders;
        }
    }
}
