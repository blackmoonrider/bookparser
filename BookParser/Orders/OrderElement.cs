﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookParser
{
    public class OrderElement
    {
        public string Book_Name { get; set; }
        public string Writer_FisrtName { get; set; }
        public string Writer_LastName { get; set; }
        public BookDetails Book_details { get; set; }
    }
}
