﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookParser
{
    public class BookDetails
    {
        public string Publishdate { get; set; }

        private decimal pagePrice;
        public decimal PagePrice
        {
            get { return Math.Round(pagePrice, 3); }
            set { pagePrice = value; }
        }
    }
}
