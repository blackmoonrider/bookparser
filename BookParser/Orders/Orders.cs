﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookParser
{
    public class Orders
    {
        public string OrderNumber { get; set; }
        public string DocDate { get; set; }
        public List<OrderElement> OrderElements { get; set; } = new List<OrderElement>();
    }
}
